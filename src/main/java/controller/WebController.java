package controller;

/**
 * Created by Artem Panasyuk on 15.05.2017.
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/test")
public class WebController {

    //@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @RequestMapping(value="/home", method = RequestMethod.GET)
    public String home(){
        return "home";
    }
    //@PreAuthorize(value = "hasRole('ROLE_ADMIN)')")
    @RequestMapping(value="/hello", method = RequestMethod.GET)
    public String welcome(){
        return "hello";
    }

/*    @RequestMapping(value={"/login"})
    public String login(){
        return "login";
    }*/

    /*@RequestMapping(value="/admin")
    public String admin(){
        return "admin";
    }

    @RequestMapping(value="/403")
    public String Error403(){
        return "403";
    }*/
}
